#include <AuroraBot/AuroraBot.h>

namespace AuroraBot {

    AuroraBot::AuroraBot(const std::string &token, const std::filesystem::path &dbPath) : SleepyDiscord::DiscordClient(token) {
        setIntents(SleepyDiscord::Intent::SERVER_MESSAGES, SleepyDiscord::Intent::SERVER_MEMBERS, SleepyDiscord::Intent::SERVERS, SleepyDiscord::Intent::SERVER_PRESENCES, SleepyDiscord::Intent::SERVER_VOICE_STATES);

        db = std::make_unique<DB::DB>(DB::GetDB(dbPath));
        db->sync_schema(true);

        //region Setting commands
        commands[Commands::SetAutoRole] = &AuroraBot::setAutoRoleCommand;
        commands[Commands::UnsetAutoRole] = &AuroraBot::unsetAutoRoleCommand;
        commands[Commands::InitSync] = &AuroraBot::initSyncCommand;
        commands[Commands::StartSync] = &AuroraBot::startSyncCommand;
        commands[Commands::SetRelation] = &AuroraBot::setRelationCommand;
        commands[Commands::ApplySync] = &AuroraBot::applySyncCommand;
        commands[Commands::CancelSync] = &AuroraBot::cancelSyncCommand;
        commands[Commands::SetCustomCategory] = &AuroraBot::setCustomCategoryCommand;
        commands[Commands::UnsetCustomCategory] = &AuroraBot::unsetCustomCategoryCommand;
        commands[Commands::CreateCustomChannel] = &AuroraBot::createCustomChannelCommand;
        commands[Commands::SetVoiceCategory] = &AuroraBot::setVoiceCategoryCommand;
        commands[Commands::UnsetVoiceCategory] = &AuroraBot::unsetVoiceCategoryCommand;
        commands[Commands::SetGamingVoiceCategory] = &AuroraBot::setGamingVoiceCategoryCommand;
        commands[Commands::UnsetGamingVoiceCategory] = &AuroraBot::unsetGamingVoiceCategoryCommand;
        commands[Commands::ActivateSavingRoles] = &AuroraBot::activateSavingRolesCommand;
        commands[Commands::DeactivateSavingRoles] = &AuroraBot::deactivateSavingRolesCommand;
        commands[Commands::ActivateApprovedRoles] = &AuroraBot::activateApprovedRolesCommand;
        commands[Commands::DeactivateApprovedRoles] = &AuroraBot::deactivateApprovedRolesCommand;
        commands[Commands::SetApprovedRoles] = &AuroraBot::setApprovedRolesCommand;
        commands[Commands::UnsetApprovedRoles] = &AuroraBot::unsetApprovedRolesCommand;
        //endregion
    }

    std::vector<DB::MemberLastRole> AuroraBot::getMemberLastRoles(const std::string &serverID, const std::string &userID) {
        return db->get_all<DB::MemberLastRole>(sqlite_orm::where(sqlite_orm::bitwise_and(sqlite_orm::is_equal(&DB::MemberLastRole::ServerID, serverID), sqlite_orm::is_equal(&DB::MemberLastRole::UserID, userID))));
    }

    void AuroraBot::updateMemberLastRoles(const std::string &serverID, const std::string &memberID, const std::vector<SleepyDiscord::Snowflake<SleepyDiscord::Role>> &memberRoles) {
        //Maybe use std::set_difference later
        std::vector<DB::MemberLastRole> memberLastRoles = getMemberLastRoles(serverID, memberID);

        std::vector<std::string> memberLastRolesToAdd;
        for (const SleepyDiscord::Snowflake<SleepyDiscord::Role> &memberRole : memberRoles) {
            if (std::find_if(memberLastRoles.begin(), memberLastRoles.end(), [&](const DB::MemberLastRole &memberLastRole){
                return memberLastRole.RoleID == memberRole;
            }) == memberLastRoles.end()) {
                memberLastRolesToAdd.emplace_back(memberRole);
            }
        }

        std::vector<std::string> memberLastRolesToDelete;
        for (const DB::MemberLastRole &memberLastRole : memberLastRoles) {
            if (std::find_if(memberRoles.begin(), memberRoles.end(), [&](const SleepyDiscord::Snowflake<SleepyDiscord::Role> &memberRole){
                return memberRole == memberLastRole.RoleID;
            }) == memberRoles.end()) {
                memberLastRolesToDelete.emplace_back(memberLastRole.RoleID);
            }
        }

        db->remove_all<DB::MemberLastRole>(sqlite_orm::where(sqlite_orm::bitwise_and(sqlite_orm::bitwise_and(sqlite_orm::is_equal(&DB::MemberLastRole::ServerID, serverID), sqlite_orm::is_equal(&DB::MemberLastRole::UserID, memberID)), sqlite_orm::in(&DB::MemberLastRole::RoleID, memberLastRolesToDelete))));
        for (const std::string &memberLastRoleToAdd : memberLastRolesToAdd) {
            db->replace(DB::MemberLastRole{serverID, memberID, memberLastRoleToAdd});
        }
    }

    //region Commands
    void AuroraBot::setAutoRoleCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::unsetAutoRoleCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::initSyncCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::startSyncCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::setRelationCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::applySyncCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::cancelSyncCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::setCustomCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::unsetCustomCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::createCustomChannelCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::setVoiceCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::unsetVoiceCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::setGamingVoiceCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::unsetGamingVoiceCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::activateSavingRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::deactivateSavingRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::activateApprovedRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::deactivateApprovedRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::setApprovedRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }

    void AuroraBot::unsetApprovedRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command) {

    }
    //endregion

    void AuroraBot::onMessage(SleepyDiscord::Message message) {
        if (message.startsWith(Commands::Prefix + ' ')) {

            //region Command splitting by space
            std::vector<std::string> command;
            std::string part;
            for (char ch: message.content) {
                if (ch != ' ') {
                    part += ch;
                } else if (!part.empty()) {
                    command.emplace_back(part);
                    part.clear();
                }
            }
            if (!part.empty()) {
                command.emplace_back(part);
            }
            //endregion

            if (commands.contains(command[1])) {
                (this->*(commands[command[1]]))(message, command);
            } else {
                sendMessage(message.channelID, Texts::WrongCommand);
            }
        }
    }

    void AuroraBot::onReady(SleepyDiscord::Ready readyData) {
        std::vector<std::string> servers;
        for (const SleepyDiscord::Server &server : getServers().vector()) {
            servers.emplace_back(server.ID);
        }
        db->remove_all<DB::Server>(sqlite_orm::where(sqlite_orm::not_in(&DB::Server::ServerID, servers)));
    }

    void AuroraBot::onDeleteServer(SleepyDiscord::UnavailableServer server) {
        db->remove<DB::Server>(server.ID.string());
    }

    void AuroraBot::onServer(SleepyDiscord::Server server) {
        std::optional<DB::Server> dbServer = db->get_optional<DB::Server>(server.ID.string());
        if (!dbServer.has_value()) {
            dbServer = DB::Server{server.ID, false, false, std::nullopt, std::nullopt, std::nullopt, std::nullopt};
            db->replace(dbServer.value());
        }

        if (dbServer.value().SaveRoles) {
            std::list<SleepyDiscord::ServerMember> serverMembers = listMembers(server.ID, 1000).list();

            if (dbServer.value().AutoRoleID.has_value()) {
                for (const SleepyDiscord::ServerMember &member : serverMembers) {
                    if (member.roles.empty() && getMemberLastRoles(server.ID, member.ID).empty()) {
                        addRole(server.ID, member.ID, dbServer.value().AutoRoleID.value());
                    }
                }
            }

            for (const SleepyDiscord::ServerMember &member : serverMembers) {
                updateMemberLastRoles(server.ID, member.ID, member.roles);
            }
        }
    }

    void AuroraBot::onMember(SleepyDiscord::Snowflake<SleepyDiscord::Server> serverID, SleepyDiscord::ServerMember member) {
        DB::Server dbServer = db->get<DB::Server>(serverID.string());
        if (dbServer.SaveRoles) {
            std::vector<DB::MemberLastRole> memberLastRoles = getMemberLastRoles(serverID, member.ID);
            if (!memberLastRoles.empty()) {
                for (const DB::MemberLastRole &memberLastRole : memberLastRoles) {
                    addRole(serverID, member.ID, memberLastRole.RoleID);
                }
            } else if (dbServer.AutoRoleID.has_value()) {
                addRole(serverID, member.ID, dbServer.AutoRoleID.value());
                updateMemberLastRoles(serverID, member.ID, member.roles);
            }
        }

    }

    void AuroraBot::onEditMember(SleepyDiscord::Snowflake<SleepyDiscord::Server> serverID, SleepyDiscord::User user, std::vector<SleepyDiscord::Snowflake<SleepyDiscord::Role>> roles, std::string nick) {
        DB::Server dbServer = db->get<DB::Server>(serverID.string());
        if (dbServer.SaveRoles) {
            updateMemberLastRoles(serverID, user.ID, roles);
        }
    }

}