#include <AuroraBot/AuroraBot.h>
#include <nlohmann/json.hpp>

int main() {
    std::fstream configFile("Config.json", std::ios::in | std::ios::binary);
    nlohmann::json config = nlohmann::json::parse(configFile);
    configFile.close();
    AuroraBot::AuroraBot bot(config["Token"].get<std::string>(), "DB.db");
    while (true) {
        try {
            bot.run();
        } catch (const std::exception &exception) {
            std::cout << "Exception: " << exception.what() << std::endl << std::flush;
        }
    }
}