cmake_minimum_required(VERSION 3.17)
project(AuroraBot VERSION 1.4.0)
include(FetchContent)

set(CMAKE_CXX_STANDARD 20)

set(CMAKE_CXX_FLAGS_RELEASE "-O3")

FetchContent_Declare(sleepy-discord GIT_REPOSITORY https://github.com/yourWaifu/sleepy-discord.git)
FetchContent_MakeAvailable(sleepy-discord)

FetchContent_Declare(sqlite_orm GIT_REPOSITORY https://github.com/fnc12/sqlite_orm.git)
FetchContent_MakeAvailable(sqlite_orm)

FetchContent_Declare(json GIT_REPOSITORY https://github.com/nlohmann/json.git GIT_TAG origin/master)
FetchContent_MakeAvailable(json)

include_directories(include)
file(GLOB_RECURSE AuroraBotSources "src/*.cpp")

add_executable(AuroraBot ${AuroraBotSources})

target_link_libraries(AuroraBot sleepy-discord sqlite_orm nlohmann_json)