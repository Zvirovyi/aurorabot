#pragma once

#include <sleepy_discord/sleepy_discord.h>
#include <AuroraBot/DB.h>
#include <vector>
#include <exception>
#include <AuroraBot/Commands.h>

namespace AuroraBot {

    class AuroraBot : public SleepyDiscord::DiscordClient {

    protected:

        std::unique_ptr<DB::DB> db;

        std::map<std::string, void (AuroraBot::*)(const SleepyDiscord::Message &, const std::vector<std::string> &)> commands;

        std::vector<DB::MemberLastRole> getMemberLastRoles(const std::string &serverID, const std::string &userID);

        void updateMemberLastRoles(const std::string &serverID, const std::string &memberID, const std::vector<SleepyDiscord::Snowflake<SleepyDiscord::Role>> &memberRoles);

        //region Commands
        void setAutoRoleCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void unsetAutoRoleCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void initSyncCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void startSyncCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void setRelationCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void applySyncCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void cancelSyncCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void setCustomCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void unsetCustomCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void createCustomChannelCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void setVoiceCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void unsetVoiceCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void setGamingVoiceCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void unsetGamingVoiceCategoryCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void activateSavingRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void deactivateSavingRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void activateApprovedRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void deactivateApprovedRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void setApprovedRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);

        void unsetApprovedRolesCommand(const SleepyDiscord::Message &message, const std::vector<std::string> &command);
        //endregion

    public:

        AuroraBot(const std::string &token, const std::filesystem::path &dbPath);

        void onMessage(SleepyDiscord::Message message) override;

        void onReady(SleepyDiscord::Ready readyData) override;

        void onDeleteServer (SleepyDiscord::UnavailableServer server) override;

        void onServer (SleepyDiscord::Server server) override;

        void onMember(SleepyDiscord::Snowflake<SleepyDiscord::Server> serverID, SleepyDiscord::ServerMember member) override;

        void onEditMember(SleepyDiscord::Snowflake<SleepyDiscord::Server> serverID, SleepyDiscord::User user, std::vector<SleepyDiscord::Snowflake<SleepyDiscord::Role>> roles, std::string nick) override;

    };

}