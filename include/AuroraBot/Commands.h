#pragma once

#include <AuroraBot/Texts.h>

namespace AuroraBot::Commands {

    const static std::string Prefix = "AB!";

    //SetAutoRole
    const static std::string SetAutoRole = "set-auto-role";

    const static std::string SetAutoRoleUsage = Texts::Usage + Prefix + ' ' + SetAutoRole + " <Идентификатор роли>";

    //UnsetAutoRole
    const static std::string UnsetAutoRole = "unset-auto-role";

    const static std::string UnsetAutoRoleUsage = Texts::Usage + Prefix + ' ' + UnsetAutoRole;

    //InitSync
    const static std::string InitSync = "init-sync";

    const static std::string InitSyncUsage = Texts::Usage + Prefix + ' ' + InitSync;

    //StartSync
    const static std::string StartSync = "start-sync";

    const static std::string StartSyncUsage = Texts::Usage + Prefix + ' ' + StartSync;

    //SetRelation
    const static std::string SetRelation = "set-relation";

    const static std::string SetRelationUsage = Texts::Usage + Prefix + ' ' + SetRelation + " <Идентификатор элемента для связывания>";

    //ApplySync
    const static std::string ApplySync = "apply-sync";

    const static std::string ApplySyncUsage = Texts::Usage + Prefix + ' ' + ApplySync;

    //CancelSync
    const static std::string CancelSync = "cancel-sync";

    const static std::string CancelSyncUsage = Texts::Usage + Prefix + ' ' + CancelSync;

    //SetCustomCategory
    const static std::string SetCustomCategory = "set-custom-category";

    const static std::string SetCustomCategoryUsage = Texts::Usage + Prefix + ' ' + SetCustomCategory + " <Идентификатор категории>";

    //UnsetCustomCategory
    const static std::string UnsetCustomCategory = "unset-custom-category";

    const static std::string UnsetCustomCategoryUsage = Texts::Usage + Prefix + ' ' + UnsetCustomCategory;

    //CreateCustomChannel
    const static std::string CreateCustomChannel = "create-custom-channel";

    const static std::string CreateCustomChannelUsage = Texts::Usage + Prefix + ' ' + CreateCustomChannel + " <Тип[voice/text]> <Название>";

    //SetVoiceCategory
    const static std::string SetVoiceCategory = "set-voice-category";

    const static std::string SetVoiceCategoryUsage = Texts::Usage + Prefix + ' ' + SetVoiceCategory + " <Идентификатор категории>";

    //UnsetVoiceCategory
    const static std::string UnsetVoiceCategory = "unset-voice-category";

    const static std::string UnsetVoiceCategoryUsage = Texts::Usage + Prefix + ' ' + UnsetVoiceCategory;

    //SetGamingVoiceCategory
    const static std::string SetGamingVoiceCategory = "set-gaming-voice-category";

    const static std::string SetGamingVoiceCategoryUsage = Texts::Usage + Prefix + ' ' + SetGamingVoiceCategory + " <Идентификатор категории>";

    //UnsetGamingVoiceCategory
    const static std::string UnsetGamingVoiceCategory = "unset-gaming-voice-category";

    const static std::string UnsetGamingVoiceCategoryUsage = Texts::Usage + Prefix + ' ' + UnsetGamingVoiceCategory;

    //ActivateSavingRoles
    const static std::string ActivateSavingRoles = "activate-saving-roles";

    const static std::string ActivateSavingRolesUsage = Texts::Usage + Prefix + ' ' + ActivateSavingRoles;

    //DeactivateSavingRoles
    const static std::string DeactivateSavingRoles = "deactivate-saving-roles";

    const static std::string DeactivateSavingRolesUsage = Texts::Usage + Prefix + ' ' + DeactivateSavingRoles;

    //ActivateApprovedRoles
    const static std::string ActivateApprovedRoles = "activate-approved-roles";

    const static std::string ActivateApprovedRolesUsage = Texts::Usage + Prefix + ' ' + ActivateApprovedRoles;

    //DeactivateApprovedRoles
    const static std::string DeactivateApprovedRoles = "deactivate-approved-roles";

    const static std::string DeactivateApprovedRolesUsage = Texts::Usage + Prefix + ' ' + DeactivateApprovedRoles;

    //SetApprovedRoles
    const static std::string SetApprovedRoles = "set-approved-roles";

    const static std::string SetApprovedRolesUsage = Texts::Usage + Prefix + ' ' + SetApprovedRoles + " <Идентификаторы ролей>...";

    //UnsetApprovedRoles
    const static std::string UnsetApprovedRoles = "unset-approved-roles";

    const static std::string UnsetApprovedRolesUsage = Texts::Usage + Prefix + ' ' + UnsetApprovedRoles;

}