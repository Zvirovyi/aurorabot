#pragma once

#include <string>

namespace AuroraBot::Texts {

    const static std::string Usage = "Использование: ";

    const static std::string WithoutGames = "без игр";

    const static std::string WrongCommand = "Неверная команда";

    const static std::string WrongParameter = "Неверный параметр";

    const static std::string Successful = "Успешно";

    const static std::string NoCopyCommand = "Вы не инициализировали команду копирования";

    const static std::string NoActiveCopyCommand = "У вас нету активных команд копирования";

    const static std::string CopyCommandAlreadyStarted = "Активная команда копирования уже существует";

    const static std::string CopyCommandWrongServer = "Уже существует активная команда копирования, связанная с другим сервером";

    const static std::string NeedToSetRelationFor = "Теперь нужно установить связь для ";

    const static std::string RelationRole = "роли ";

    const static std::string RelationChannel = "канала ";

    const static std::string NoNeedForSettingRelations = "Нету ролей или каналов, которым нужно установить связь";

    const static std::string SetRelationComplete = "Установка связей завершена, теперь вы можете применить копирование";

    const static std::string AutoRoleNeedsSaveRoles = "Для включения функции автоматической роли требуется включённая функция сохранения ролей";

}